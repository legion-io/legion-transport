# Legion::Transport ChangeLog

## v1.1.9
* Adding new setting `transport.channel.default_worker_pool_size = 1` for whenever a new channel is created
* Adding new setting `transport.channel.session_worker_pool_size = 1` for default session channel
* Making the following settings setable with env variables
    - transport.prefetch
    - transport.logger_level
    - transport.channel.default_worker_pool_size
    - transport.channel.session_worker_pool_size
    - transport.connection.user
    - transport.connection.password
    - transport.connection.host
    - transport.connection.port
    - transport.connection.vhost

## v1.1.8
* Removing codecov gem
* Changing bunny gem requirement to `>= 2.17.0`
* Changing current-ruby gem requirement to `>= 1.1.7`
* Removing JRuby support
* Removing bin/ folder
* Cleaning up some channel objects inside Messages and Queues

## v1.1.3
* Adding JRuby support via a different gem
* Adding more logic on choosing which gem to pick for the CONNECTOR
* Cleaning up some rubocop things

## v1.1.2
Updating queue auto_delete to be false