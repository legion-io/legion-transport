require_relative 'transport/version'
require_relative 'transport/settings'

module Legion
  module Transport
    begin
      require 'march_hare'
      TYPE = 'march_hare'.freeze
      CONNECTOR = ::MarchHare
    rescue LoadError
      require 'bunny'
      TYPE = 'bunny'.freeze
      CONNECTOR = ::Bunny
    end
  end

  require_relative 'transport/common'
  require_relative 'transport/queue'
  require_relative 'transport/exchange'
  require_relative 'transport/message'
end
