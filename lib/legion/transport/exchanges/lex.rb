module Legion
  module Transport
    module Exchanges
      class Lex < Legion::Transport::Exchange
        def exchange_name
          'lex'
        end
      end
    end
  end
end
