require 'legion/transport/exchanges/lex'

module Legion
  module Transport
    module Messages
      class NodeHealth < Legion::Transport::Message
        def exchange
          Legion::Transport::Exchanges::Node
        end

        def routing_key
          'health'
        end

        def validate
          @valid = true
        end
      end
    end
  end
end
