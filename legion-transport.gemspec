lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/transport/version'

Gem::Specification.new do |spec|
  spec.name = 'legion-transport'
  spec.version       = Legion::Transport::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Used by Legion to connect with RabbitMQ'
  spec.description   = 'The Legion transport gem'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-transport'
  spec.required_ruby_version = '>= 2.5.0'

  spec.metadata = {
    'bug_tracker_uri' => 'https://legionio.atlassian.net/projects/TRANSPORT/issues',
    'changelog_uri' => 'https://bitbucket.org/legion-io/legion-transport/src/master/CHANGELOG.md',
    'homepage_uri' => 'https://bitbucket.org/legion-io/legion-transport',
    'wiki_uri' => 'https://bitbucket.org/legion-io/legion-transport/wiki/Home'
  }

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'legion-logging'
  spec.add_development_dependency 'legion-settings'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'simplecov_json_formatter'

  spec.add_dependency 'bunny', '>= 2.17.0'
  spec.add_dependency 'concurrent-ruby', '>= 1.1.7'
  spec.add_dependency 'legion-json'
end
